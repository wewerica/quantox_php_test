# README #

### Requirements ###

* PHP 5.5+ / HHVM
* Mysql 5+
* Composer

### How to set up? ###

```
#!shell

git clone https://wewerica@bitbucket.org/wewerica/quantox_php_test.git
```


```
#!shell

cd quantox
```


```
#!shell

composer install
```

_Make MySQL table_



### SQL table ###
```
#!sql

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;

```

_Make .htacces file if missing_
### .htaccess ###
```
<IfModule mod_rewrite.c>
    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>

```